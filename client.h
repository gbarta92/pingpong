#ifndef CLIENT_H
#define CLIENT_H

#include <sys/types.h>


class Client {
public:
    Client(char *server_host, char *server_port);
    virtual void operate() = 0;
protected:
    ssize_t write(char *buffer, size_t count);
    ssize_t read(char *buffer, size_t count);
private:
    int sock_fd;
};

#endif
