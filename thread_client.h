#ifndef THREAD_CLIENT_H
#define THREAD_CLIENT_H

#include "client.h"

#include <mutex>


class ThreadClient : public Client {
public:
    ThreadClient(char *server_host, char *server_port);
    virtual void operate() override;
private:
    char buffer[100];
    std::mutex m;
};


#endif
