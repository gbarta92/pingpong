#include "client.h"

#include <iostream>
#include <cstring>

#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>


ssize_t Client::write(char *buffer, size_t count) {
    return ::write(sock_fd, buffer, count);
}

ssize_t Client::read(char *buffer, size_t count) {
    return ::read(sock_fd, buffer, count);
}

Client::Client(char *server_host, char *server_port) : sock_fd(socket(AF_INET, SOCK_STREAM, 0)) {
    struct addrinfo hints, *result;
    memset(&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    int s = getaddrinfo(server_host, server_port, &hints, &result);
    if (s != 0) {
        std::cerr << "getaddrinfo: " << gai_strerror(s) << std::endl;
        exit(EXIT_FAILURE);
    }

    if (connect(sock_fd, result->ai_addr, result->ai_addrlen) == -1) {
        perror("connect()");
        exit(EXIT_FAILURE);
    }
}
