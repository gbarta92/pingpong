project(pingpong)
cmake_minimum_required(VERSION 3.5)

set(CXX_STANDARD 14)

find_package(Threads)

add_executable(thread_server server.cpp server.h thread_server.cpp thread_server.h)
add_executable(thread_client client.cpp client.h thread_client.cpp thread_client.h)

target_link_libraries(thread_server ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(thread_client ${CMAKE_THREAD_LIBS_INIT})
