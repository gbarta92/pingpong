#ifndef THREAD_SERVER_H
#define THREAD_SERVER_H

#include "server.h"

#include <thread>
#include <vector>


class ThreadServer : public Server {
public:
    ThreadServer(char *port);
    virtual void operate() override;
private:
    std::vector<std::thread> conn_handlers;
};


#endif
