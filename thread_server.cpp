#include "thread_server.h"

#include <iostream>

#include <unistd.h>


ThreadServer::ThreadServer(char* port) : Server(port) {}

void ThreadServer::operate() {
    while(1) {
        std::cout << "Waiting for connection...\n";
        int client_fd = accept();
        std::cout << "Connection made: client_fd=" << client_fd << std::endl;

        conn_handlers.emplace_back([](int client_fd) {
            while(1) {
                char buffer[1000];
                int len = read(client_fd, buffer, sizeof (buffer) - 1);
                buffer[len] = '\0';

                std::cout << "Read " << len << " chars\n";
                std::cout << "===\n";
                std::cout << "Message: " << buffer << std::endl;

                write(client_fd, buffer, len);
            }
        }, client_fd);
    }
}

int main(int argc, char **argv) {
    if (argc < 1) {
        std::cout << "Please provide a port number.\n";
        return 1;
    }

    ThreadServer server(argv[1]);

    server.operate();

    return 0;
}
