#ifndef SERVER_H
#define SERVER_H


class Server {
public:
    Server(char *part);
    virtual void operate() = 0;
protected:
    int accept();
private:
    int sock_fd;
};

#endif
