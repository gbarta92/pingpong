#include "thread_client.h"

#include <iostream>
#include <cstring>
#include <thread>

#include <unistd.h>

#define READ_BUFFER_LEN 100


ThreadClient::ThreadClient(char *server_host, char *server_port)
    : Client(server_host, server_port) {}

void ThreadClient::operate() {
    m.lock();

    std::thread user_io([this]() {
        while(1) {
            std::cin >> buffer;
            m.unlock();
        }
    });

    std::thread server_io([this]() {
        while(1) {
            m.lock();

            std::cout << "SENDING: " << buffer << std::endl;
            std::cout << "===\n";

            if (write(buffer, strlen(buffer)) == -1) {
                std::cerr << "Could not write socket." << std::endl;
                break;
            }

            ssize_t len;
            char resp[READ_BUFFER_LEN];
            if (len = read(resp, strlen(buffer) - 1) == -1) {
                std::cerr << "Could not read from socket." << std::endl;
                break;
            }

            resp[len] = '\0';
            std::cout << "Got back: " << buffer << std::endl;
        }
    });

    server_io.join();
    user_io.join();
}

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Please provide a hostname and a port number.\n";
        return 1;
    }

    ThreadClient client(argv[1], argv[2]);

    client.operate();

    return 0;
}
