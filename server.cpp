#include "server.h"

#include <iostream>
#include <cstring>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define BACKLOG (5)


Server::Server(char *host) : sock_fd(socket(AF_INET, SOCK_STREAM, 0)) {
    addrinfo hints, *result;
    memset(&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int s = getaddrinfo(NULL, host, &hints, &result);
    if (s != 0) {
        std::cerr << "getaddrinfo: " << gai_strerror(s) << std::endl;
        exit(EXIT_FAILURE);
    }

    if (bind(sock_fd, result->ai_addr, result->ai_addrlen) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    if (listen(sock_fd, BACKLOG) != 0) {
        perror("listen()");
        exit(EXIT_FAILURE);
    }

    sockaddr_in *result_addr = (sockaddr_in *)(result->ai_addr);
    std::cout << "Listening on file descriptor " << sock_fd
              << ", port " << ntohs(result_addr->sin_port) << std::endl;

    freeaddrinfo(result);
}

int Server::accept() {
    return ::accept(sock_fd, NULL, NULL);
}
